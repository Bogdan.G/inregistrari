from django import forms

from .models import Client


class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ['last_name', 'first_name', 'cnp']

        widgets = {
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'cnp': forms.TextInput(attrs={'class': 'form-control'}),
        }
