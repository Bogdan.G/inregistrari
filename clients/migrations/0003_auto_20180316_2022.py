# Generated by Django 2.0.3 on 2018-03-16 18:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0002_auto_20180316_1618'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='cnp',
            field=models.CharField(max_length=13, validators=[django.core.validators.RegexValidator(code='invalid_cnp', message='CNP-ul trebuie sa contina 13 cifre', regex='\\d{13}')], verbose_name='cnp'),
        ),
        migrations.AlterField(
            model_name='client',
            name='first_name',
            field=models.CharField(max_length=50, validators=[django.core.validators.RegexValidator(code='invalid_username', message='Format invalid', regex='[a-zA-Z\\s-]+$')], verbose_name='prenume'),
        ),
        migrations.AlterField(
            model_name='client',
            name='last_name',
            field=models.CharField(max_length=50, validators=[django.core.validators.RegexValidator(code='invalid_username', message='Format invalid', regex='[a-zA-Z\\s-]+$')], verbose_name='nume'),
        ),
    ]
