from django.urls import path

from . import views

urlpatterns = [
    path('<int:pk>/', views.ClientUpdate.as_view(),
         name='client-update'),
    path('rezultate-cautare/', views.search_results, name='search-results'),
    path('', views.clients_view, name='clients'),
]
