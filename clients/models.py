from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator


class Client(models.Model):

    nume_validator = RegexValidator(regex='[a-zA-Z\s-]+$', message="Format invalid",
                                    code='invalid_username')
    cnp_validator = RegexValidator(regex='\d{13}', message="CNP-ul trebuie sa contina 13 cifre",
                                   code='invalid_cnp')

    client_id = models.CharField(
        'id client', max_length=50, editable=False, unique=True)
    last_name = models.CharField(
        'nume', max_length=50, validators=[nume_validator])
    first_name = models.CharField(
        'prenume', max_length=50, validators=[nume_validator])
    cnp = models.CharField('cnp', max_length=13, validators=[cnp_validator])

    class Meta:
        verbose_name = 'client'
        verbose_name_plural = 'clienti'

    def __str__(self):
        return f'{self.last_name.capitalize()}-{self.first_name.capitalize()}'

    def get_absolute_url(self):
        return reverse('clients:clients')
