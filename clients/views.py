from django.shortcuts import render
from django.views.generic import UpdateView
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Q

from datetime import datetime
import openpyxl

from .models import Client
from .forms import ClientForm


def clients_view(request):

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ClientForm(request.POST)
        clients = Client.objects.all().order_by('-id')
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required

            client_id = datetime.now().strftime('%d%m%y') + \
                str(len(Client.objects.all()) + 1)

            client = Client(client_id=client_id,
                            last_name=form.cleaned_data['last_name'],
                            first_name=form.cleaned_data['first_name'],
                            cnp=form.cleaned_data['cnp'])
            client.save()
            # redirect to a new URL:
            return HttpResponseRedirect('.')

        else:

            return render(request, 'clients/clients.html',
                          {'form': form,
                           'clients': clients, })

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ClientForm()
        clients = Client.objects.all().order_by('-id')

    return render(request, 'clients/clients.html',
                  {'form': form,
                   'clients': clients, })


class ClientUpdate(UpdateView):
    model = Client
    template_name = 'clients/client_form.html'
    form_class = ClientForm


def search_results(request):

    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']

        search_term = Q(last_name__icontains=q) | Q(
            first_name__icontains=q) | Q(cnp__icontains=q)
        clients = Client.objects.filter(search_term)

        return render(request, 'clients/search-results.html',
                      {'clients': clients,
                       'query': q})

    else:
        return HttpResponse('Introduceti un termen de cautare.')


def to_excel(request):

    clients_ids = []
    clients_last_names = []
    clients_first_names = []
    clients_cnps = []

    for client in Client.objects.all():
        clients_ids.append(client.client_id)
    for client in Client.objects.all():
        clients_last_names.append(client.last_name)
    for client in Client.objects.all():
        clients_first_names.append(client.first_name)
    for client in Client.objects.all():
        clients_cnps.append(client.cnp)

    wb = openpyxl.Workbook()
    sheet = wb['Sheet']

    for row in range(1, len(Client.objects.all())):
        for col in range(1, 5):
            value = [clients_ids[row - 1],
                     clients_last_names[row - 1],
                     clients_first_names[row - 1],
                     clients_cnps[row - 1],
                     ]
            sheet.cell(row=row, column=col).value = value[col - 1]

    wb.save('clients.xlsx')

    return HttpResponseRedirect('.')
